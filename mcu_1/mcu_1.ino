/*
 * Sittidach Kaeorawang
 * 622021129@tsu.ac.th
 */
#include <TimerOne.h>
#include <OneWire.h>

#define PIN_BRIGHTNESS A0
#define PIN_TEMPERATURE 2
#define PIN_BUTTON_SWITCH_1 4
#define PIN_BUTTON_SWITCH_2 5
#define PIN_BUTTON_SWITCH_3 6
#define PIN_BUTTON_SWITCH_4 7

typedef union
{
  byte raw[12];
  struct
  {
    float brightness;
    float temperature;
    bool button_switch[4];
  };
} data_input_t;

OneWire ds18b20(PIN_TEMPERATURE);
int temperature_data;
data_input_t data_input_all;

void setup()
{
  Timer1.initialize(350000);
  Timer1.attachInterrupt(tempertureRequest);
  Serial.begin(9600);
  tempertureRequest();
  pinMode(PIN_BUTTON_SWITCH_1, INPUT_PULLUP);
  pinMode(PIN_BUTTON_SWITCH_2, INPUT_PULLUP);
  pinMode(PIN_BUTTON_SWITCH_3, INPUT_PULLUP);
  pinMode(PIN_BUTTON_SWITCH_4, INPUT_PULLUP);
  delay(2500);
}

void loop()
{
  if (!digitalRead(PIN_BUTTON_SWITCH_1) || !digitalRead(PIN_BUTTON_SWITCH_2) || !digitalRead(PIN_BUTTON_SWITCH_3) || !digitalRead(PIN_BUTTON_SWITCH_4))
  {
    data_input_all.button_switch[0] = !digitalRead(PIN_BUTTON_SWITCH_1);
    data_input_all.button_switch[1] = !digitalRead(PIN_BUTTON_SWITCH_2);
    data_input_all.button_switch[2] = !digitalRead(PIN_BUTTON_SWITCH_3);
    data_input_all.button_switch[3] = !digitalRead(PIN_BUTTON_SWITCH_4);
    data_input_all.brightness = brightnessRead();
    data_input_all.temperature = tempertureRead();

    Serial.write(data_input_all.raw, 12);
    delay(350);
  }
}
void tempertureRequest() {
  ds18b20.reset();     //If Device ACK on Reset
  ds18b20.write(0xCC); //Skip ROM Command
  ds18b20.write(0x44); //Start Conversion Temp
}

float tempertureRead()
{
  ds18b20.reset();     //Re-Start New Command
  ds18b20.write(0xCC); //Skip ROM Command
  ds18b20.write(0xBE); //Read Scratchpad RAM(Temperature Data)
  
  temperature_data = ds18b20.read();
  temperature_data |= ds18b20.read() << 8;
  return temperature_data * 0.0625;
}

float brightnessRead()
{
  return analogRead(PIN_BRIGHTNESS) * 5 / 1023.0;
}
