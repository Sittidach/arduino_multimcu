/*
 * Sittidach Kaeorawang
 * 622021129@tsu.ac.th
 */
#include <TimerOne.h>
#include <LiquidCrystal_I2C.h>

#define PIN_LED 10
#define PIN_MOTOR 12
#define ADDRESS_LCD_1 0x20
#define ADDRESS_LCD_2 0x21

typedef union
{
  byte raw[12];
  struct
  {
    float brightness;
    float temperature;
    bool button_switch[4];
  };
} data_input_t;


LiquidCrystal_I2C lcd1(ADDRESS_LCD_1, 20, 4);
LiquidCrystal_I2C lcd2(ADDRESS_LCD_2, 20, 4);

data_input_t data_input_all;

void setup()
{
  lcd2.init();
  lcd2.backlight();
  lcd2.setCursor(0, 0);
  lcd2.print("622021129");

  lcd1.init();
  lcd1.backlight();
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_MOTOR, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  if (Serial.readBytes(data_input_all.raw, 12))
  {
    if (data_input_all.button_switch[0]) {
      digitalWrite(PIN_LED, HIGH);
    }
    else if (data_input_all.button_switch[1]) {
      digitalWrite(PIN_LED, LOW);
    }

    if (data_input_all.button_switch[2]) {
      digitalWrite(PIN_MOTOR, HIGH);
    }
    else if (data_input_all.button_switch[3]) {
      digitalWrite(PIN_MOTOR, LOW);
    }
    
    lcd1.setCursor(0, 0);
    lcd1.print(data_input_all.temperature, 2);
  }
}
