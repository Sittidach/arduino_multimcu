/*
 * Sittidach Kaeorawang
 * 622021129@tsu.ac.th
 */
#define SEVEN_SEGMENT_DIGIT_DELAY 1
#define SEVEN_SEGMENT_COMMON_CATHODE false
#define SEVEN_SEGMENT_DIGIT_SIZE  4

typedef union
{
  byte raw[12];
  struct
  {
    float brightness;
    float temperature;
    bool button_switch[4];
  };
} data_input_t;

int pin_seven_segment_number[8] = {8, 9, 10, 11, 12, 13, 7, 6};
int pin_seven_segment_digit[4]  = {5, 4, 3, 2};
int seven_segment_dict[10] = {
  [0] = 0x3F,
  [1] = 0x06,
  [2] = 0x5B,
  [3] = 0x4F,
  [4] = 0x66,
  [5] = 0x6D,
  [6] = 0x7D,
  [7] = 0x07,
  [8] = 0x7F,
  [9] = 0x6F,
};
data_input_t data_input_all;
float brightness;

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(4);
  pinMode(pin_seven_segment_number[0], OUTPUT);
  pinMode(pin_seven_segment_number[1], OUTPUT);
  pinMode(pin_seven_segment_number[2], OUTPUT);
  pinMode(pin_seven_segment_number[3], OUTPUT);
  pinMode(pin_seven_segment_number[4], OUTPUT);
  pinMode(pin_seven_segment_number[5], OUTPUT);
  pinMode(pin_seven_segment_number[6], OUTPUT);
  pinMode(pin_seven_segment_number[7], OUTPUT);

  pinMode(pin_seven_segment_digit[0], OUTPUT);
  pinMode(pin_seven_segment_digit[1], OUTPUT);
  pinMode(pin_seven_segment_digit[2], OUTPUT);
  pinMode(pin_seven_segment_digit[3], OUTPUT);
}

void loop() {
  sevenSegmentPrintFloat(brightness, 2);
  if (Serial.readBytes(data_input_all.raw, 12)) {
    brightness = data_input_all.brightness;
  }
}

void sevenSegmentPrintFloat(float number, int decimal_places) {

  float number_temp = number;
  for (int i = 0; i < decimal_places; i++) {
    number_temp *= 10;
  }

  number_temp += 0.5; // Round up.

  for (int i = SEVEN_SEGMENT_DIGIT_SIZE - 1; i >= 0; i--) {
    sevenSegmentWrite(seven_segment_dict[(int)number_temp % 10], i);
    number_temp /= 10;
    delay(SEVEN_SEGMENT_DIGIT_DELAY);
  }

  sevenSegmentWrite(seven_segment_dict[(int)number % 10] | 0x80, SEVEN_SEGMENT_DIGIT_SIZE - decimal_places - 1);
  delay(SEVEN_SEGMENT_DIGIT_DELAY);
}

void sevenSegmentWrite(int seven_segment_number, int seven_segment_digit) {
  if (!SEVEN_SEGMENT_COMMON_CATHODE) {
    seven_segment_number = ~seven_segment_number;
  }

  for (int i = 0; i < SEVEN_SEGMENT_DIGIT_SIZE; i++) {
    digitalWrite(pin_seven_segment_digit[i], SEVEN_SEGMENT_COMMON_CATHODE);
  }

  digitalWrite(pin_seven_segment_number[0], seven_segment_number & 1);
  digitalWrite(pin_seven_segment_number[1], seven_segment_number & 2);
  digitalWrite(pin_seven_segment_number[2], seven_segment_number & 4);
  digitalWrite(pin_seven_segment_number[3], seven_segment_number & 8);
  digitalWrite(pin_seven_segment_number[4], seven_segment_number & 16);
  digitalWrite(pin_seven_segment_number[5], seven_segment_number & 32);
  digitalWrite(pin_seven_segment_number[6], seven_segment_number & 64);
  digitalWrite(pin_seven_segment_number[7], seven_segment_number & 128);

  digitalWrite(pin_seven_segment_digit[seven_segment_digit], !SEVEN_SEGMENT_COMMON_CATHODE);
}
